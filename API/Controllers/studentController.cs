﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class studentController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage studentlogin(stuLogin login)
        {
            userTable dto = new userTable();
            dto = null;

            dataContext dc = new dataContext();

            var data = dc.userTable.Where(x => x.userName == login.userName && x.password == login.password).ToArray();
            dto = dc.userTable.FirstOrDefault(x => x.userName == login.userName && x.password == login.password);
            if (dto != null)
            {
                if (data.Any(u => u.userName == login.userName && u.password == login.password && u.roleId == 3))
                {
                    var data2 = dc.studentTable.FirstOrDefault(x=>x.userId==dto.userId);
                    if(data2!=null)
                    {
                        var data3 = dc.locationTable.FirstOrDefault(x=>x.busId==data2.busId);
                        //List<double> location = new List<double>();
                        //location.Add(data3.latitude);
                        //location.Add(data3.longitude);
                        string location = Convert.ToString(data3.latitude) + "," + Convert.ToString(data3.longitude);
                        return Request.CreateResponse(HttpStatusCode.OK, location);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "not found,0");
                    }
                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.OK, "not found,0");
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, "not found,0");
            }
        }
    }
    public class stuLogin
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
