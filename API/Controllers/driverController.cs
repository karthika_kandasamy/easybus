﻿using Data;
using Data.DTO;
using Data.Entities;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class driverController : ApiController
    {
        
        [HttpPost]
        public HttpResponseMessage driverlogin(Login login)
        {
            userTable dto = new userTable();
            dto = null;

            dataContext dc = new dataContext();

            var data = dc.userTable.Where(x => x.userName == login.userName && x.password == login.password).ToArray();
            dto = dc.userTable.FirstOrDefault(x => x.userName == login.userName && x.password == login.password);
            if (dto != null)
            {
                if (data.Any(u => u.userName == login.userName && u.password == login.password && u.roleId == 2 && u.status == 0))
                {
                    userOperation oper = new userOperation();
                    oper.statusupdate(dto.userId);
                    var list = new List<returnInfo>();
                    list.Add(new returnInfo() {userId=dto.userId,userName=dto.userName  });
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
                else
                {
                    string notfound = "not found";
                    return Request.CreateResponse(HttpStatusCode.OK, notfound);
                }
            }
            else
            {
                string notfound = "not found";
                return Request.CreateResponse(HttpStatusCode.OK, notfound);
            }
        }
    }
    public class Login
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
    public class returnInfo
    {
        public string userId { get; set; }
        public string userName { get; set; }
    }
}

