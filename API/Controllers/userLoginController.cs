﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class userLoginController : ApiController
    {
       

        [HttpPost]
        public HttpResponseMessage login(user ul)
        {
            dataContext dc = new dataContext();
            var data = dc.userTable.Where(x => x.userName == ul.userName && x.password == ul.password).ToArray();
            var dto = dc.userTable.FirstOrDefault(x => x.userName == ul.userName && x.password == ul.password);
            try
            {
                if (dto != null)
                {
                    if (data.Any(u => u.userName == ul.userName && u.password == ul.password && u.roleId == 3))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, dto.userName + "," + dto.userId);                         
                    }
                    else if(data.Any(u => u.userName == ul.userName && u.password == ul.password && u.roleId == 2))
                    {
                        if(dto.status == 1)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "already");

                        }
                        else
                        {
                            dto.status = 1;
                            dc.Entry(dto).State = EntityState.Modified;
                            dc.SaveChanges();
                            return Request.CreateResponse(HttpStatusCode.OK, dto.userName + "," + dto.userId);

                        }
                        
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
                    }
                }
                else
                {
                     return Request.CreateResponse(HttpStatusCode.OK, "unsuccess"); //
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }
        public class user
        {
            public string userName { get; set; }
            public string password { get; set; }
        }

 
}