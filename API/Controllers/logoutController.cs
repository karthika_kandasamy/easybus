﻿using Data;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class logoutController : ApiController
    {
        
            [HttpPost]
            public HttpResponseMessage driverlogout(enddriver ed)
            {
                try
                {
                    dataContext dc = new dataContext();

                    var dto1 = dc.userTable.FirstOrDefault(x => x.userId == ed.userId);
                    userOperation operation = new userOperation();
                    operation.statusreverse(dto1.userName);

                    ////var dto2 = dc.busTable.FirstOrDefault(x => x.busId == ed.busId);
                    //busOperation oper = new busOperation();
                    //oper.statusreverse(ed.busId);

                    return Request.CreateResponse(HttpStatusCode.OK, "success");
                }
                catch (Exception e)
                {

                    return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
                }
            }
        }
        public class enddriver
        {
            public string userId { get; set; }
            //public int busId { get; set; }
        }
    
}
