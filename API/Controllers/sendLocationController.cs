﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class sendLocationController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage location(buss b1)
        {
            try
            {
                
                dataContext dc = new dataContext();
                var busRow = dc.busTable.FirstOrDefault(x =>x.busId==b1.busId);
                if (busRow.status == 1)
                {
                    var loc = dc.locationTable.FirstOrDefault(x => x.busId == busRow.busId);
                    var routeRow = dc.routeTable.FirstOrDefault(x => busRow.routeId == x.routeId);

                    return Request.CreateResponse(HttpStatusCode.OK, busRow.busId + "," + routeRow.source + "," + routeRow.via + "," + routeRow.destination + "," + loc.latitude + "," + loc.longitude);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, busRow.busId+",No,No,No,"+ 11.4280598+","+ 77.6701069);
                }
            }
            catch (Exception err)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess"); //,err);

            }
        }

    }
    public class buss
    {
        public string busId { get; set; }
    }

    
   

}
