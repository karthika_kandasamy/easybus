﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class driverLogoutController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage logout(userLogout ul)
        {
            try
            {
                dataContext dc = new dataContext();
                var dto = dc.userTable.FirstOrDefault(x => x.userId == ul.userId);
                dto.status = 0;
                dc.Entry(dto).State = EntityState.Modified;
                dc.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, "success");
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
        public class userLogout
        {
            public string userName { get; set; }
            public string userId { get; set; }
        }
    }
}
