﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class busInfoController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage start(busid b)
        {
            try
            {
                dataContext dc = new dataContext();
                var busRow = dc.busTable.FirstOrDefault(x => b.busId == x.busId);
                var routeRow = dc.routeTable.FirstOrDefault(x => busRow.routeId == x.routeId);
                return Request.CreateResponse(HttpStatusCode.OK, b.busId + "," + routeRow.plannedTime + "," + routeRow.source + "," + routeRow.via + "," + routeRow.destination);
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }
    public class busid
    {
        public string busId { get; set; }
   }
}
