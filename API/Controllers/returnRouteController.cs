﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class returnRouteController : ApiController
    {


        public HttpResponseMessage returnBusRoute(busRoute br)
        {
            dataContext dc = new dataContext();
            busTable bt = new busTable();
            routeTable rt = new routeTable();
            try
            {
                var busRow = dc.busTable.FirstOrDefault(x => x.busId == br.busId);
                var busRoute = busRow.routeId;
                var routeRow = dc.routeTable.FirstOrDefault(x => x.routeId == busRoute);
                var routeList = new List<returnBusRoute>();

                routeList.Add(new returnBusRoute { destination = routeRow.destination, via = routeRow.via });
                return Request.CreateResponse(HttpStatusCode.OK, routeList);
            }
            catch (Exception err)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "No routes for given bus");
            }
        }


    }

    public class busRoute
    {

        public int busId { get; set; }

    }


    public class returnBusRoute
    {

        public string destination { get; set; }
        public string via { get; set; }

    }
}
