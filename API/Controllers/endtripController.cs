﻿using Data;
using Data.Entities;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class endTripController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage end(endClass e)
        {
            try
            {
                dataContext dc = new dataContext();
                busTable b = dc.busTable.FirstOrDefault(x => x.busId.Equals(e.busId));
                b.status = 0;
                dc.busTable.Attach(b);
                dc.Entry(b).State = EntityState.Modified;
                dc.SaveChanges();
                var start = dc.actualbusTimes.FirstOrDefault(x => x.busId.Equals(e.busId));
                start.endtime = DateTime.Now.ToShortTimeString();
                dc.actualbusTimes.Attach(start);
                dc.Entry(start).State = EntityState.Modified;
                dc.SaveChanges();
                locationTable lt = dc.locationTable.FirstOrDefault(m=>m.busId==e.busId);
                dc.locationTable.Remove(lt);
                dc.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, "success");
            }
            catch(Exception er)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
            
        }
        public class endClass
        {
            public string userId { get; set; }
            public string busId { get; set; }
            
        }
    }
}
