﻿using Data.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class busController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage busInfo()
        {
                busOperation bo = new busOperation();
                var result = bo.availBusDetails();
                List<buslist> listName = new List<buslist>();
                foreach (var i in result)
                {
                if(i.status==0)
                    listName.Add(new buslist() { busId = i.busId });
                }
                //int index;
                //for (index = 0; index < listName.Count - 1; index++)
                //{
                //    s += listName[index];
                //    s += "%";
                //}
                //s += listName[index];

                return Request.CreateResponse(HttpStatusCode.OK, listName);
         }
    }
   
    public class buslist
    {
        public int busId { get; set; }
    }
}
