﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class searchController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage search(busid b)
        {
            try
            {
                dataContext dc = new dataContext();
                var busRow = dc.busTable.FirstOrDefault(x => b.busId == x.busId);
               if(busRow.status==0)
                return Request.CreateResponse(HttpStatusCode.OK, "success");
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
                }
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }
}
