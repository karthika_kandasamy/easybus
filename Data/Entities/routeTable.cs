﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class routeTable
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int routeId { get; set; }
        [DataType("varchar(50)")]
        public string source { get; set; }
        [DataType("varchar(50)")]
        public string destination { get; set; }
        [DataType("varchar(50)")]
        public string via { get; set; }
        [DataType("varchar(50)")]
        public string plannedTime { get; set; }

    }
}
