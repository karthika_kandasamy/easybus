﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class alternateBusTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataType("int")]
        public string busId { get; set; }
        [DataType("int")]
        public string  alternateBusId { get; set; }
    }
}
