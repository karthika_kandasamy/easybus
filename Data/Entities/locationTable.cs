﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class locationTable
    {
        [Key]
        [DataType("integer")]
        public string  busId { get; set; }
        [DataType("varchar(20)")]
        public string userId { get; set; }
        [DataType("varchar(20)")]
        public double latitude { get; set; }
        [DataType("varchar(20)")]
        public double longitude { get; set; }
        [DataType("DateTime")]
        public DateTime dateTime { get; set; }

        [ForeignKey("busId")]
        public virtual busTable busTable { get; set; }

        [ForeignKey("userId")]
        public virtual userTable userTable { get; set; }

    }
}
