namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class easybus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.actualbusTimes",
                c => new
                    {
                        busId = c.String(nullable: false, maxLength: 128),
                        starttime = c.String(),
                        endtime = c.String(),
                    })
                .PrimaryKey(t => t.busId);
            
            CreateTable(
                "dbo.alternateBusTables",
                c => new
                    {
                        busId = c.String(nullable: false, maxLength: 128),
                        alternateBusId = c.String(),
                    })
                .PrimaryKey(t => t.busId);
            
            CreateTable(
                "dbo.busTables",
                c => new
                    {
                        busId = c.String(nullable: false, maxLength: 128),
                        busNo = c.String(nullable: false, maxLength: 12),
                        routeId = c.Int(nullable: false),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.busId)
                .ForeignKey("dbo.routeTables", t => t.routeId, cascadeDelete: true)
                .Index(t => t.routeId);
            
            CreateTable(
                "dbo.routeTables",
                c => new
                    {
                        routeId = c.Int(nullable: false),
                        source = c.String(),
                        destination = c.String(),
                        via = c.String(),
                        plannedTime = c.String(),
                    })
                .PrimaryKey(t => t.routeId);
            
            CreateTable(
                "dbo.departmentTables",
                c => new
                    {
                        deptno = c.Int(nullable: false),
                        deptname = c.String(),
                    })
                .PrimaryKey(t => t.deptno);
            
            CreateTable(
                "dbo.locationTables",
                c => new
                    {
                        busId = c.String(nullable: false, maxLength: 128),
                        userId = c.String(maxLength: 128),
                        latitude = c.Double(nullable: false),
                        longitude = c.Double(nullable: false),
                        dateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.busId)
                .ForeignKey("dbo.busTables", t => t.busId)
                .ForeignKey("dbo.userTables", t => t.userId)
                .Index(t => t.busId)
                .Index(t => t.userId);
            
            CreateTable(
                "dbo.userTables",
                c => new
                    {
                        userId = c.String(nullable: false, maxLength: 128),
                        userName = c.String(),
                        firstName = c.String(),
                        password = c.String(),
                        roleId = c.Int(nullable: false),
                        status = c.Int(nullable: false),
                        Phonenumber = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.userId)
                .ForeignKey("dbo.roleTables", t => t.roleId, cascadeDelete: true)
                .Index(t => t.roleId);
            
            CreateTable(
                "dbo.roleTables",
                c => new
                    {
                        roleno = c.Int(nullable: false),
                        rolename = c.String(),
                    })
                .PrimaryKey(t => t.roleno);
            
            CreateTable(
                "dbo.studentTables",
                c => new
                    {
                        userId = c.String(nullable: false, maxLength: 128),
                        studentName = c.String(nullable: false),
                        parentName = c.String(nullable: false),
                        parentPhonenumber = c.Long(nullable: false),
                        busId = c.String(nullable: false, maxLength: 128),
                        departmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.userId)
                .ForeignKey("dbo.busTables", t => t.busId, cascadeDelete: true)
                .ForeignKey("dbo.departmentTables", t => t.departmentId, cascadeDelete: true)
                .Index(t => t.busId)
                .Index(t => t.departmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.studentTables", "departmentId", "dbo.departmentTables");
            DropForeignKey("dbo.studentTables", "busId", "dbo.busTables");
            DropForeignKey("dbo.locationTables", "userId", "dbo.userTables");
            DropForeignKey("dbo.userTables", "roleId", "dbo.roleTables");
            DropForeignKey("dbo.locationTables", "busId", "dbo.busTables");
            DropForeignKey("dbo.busTables", "routeId", "dbo.routeTables");
            DropIndex("dbo.studentTables", new[] { "departmentId" });
            DropIndex("dbo.studentTables", new[] { "busId" });
            DropIndex("dbo.userTables", new[] { "roleId" });
            DropIndex("dbo.locationTables", new[] { "userId" });
            DropIndex("dbo.locationTables", new[] { "busId" });
            DropIndex("dbo.busTables", new[] { "routeId" });
            DropTable("dbo.studentTables");
            DropTable("dbo.roleTables");
            DropTable("dbo.userTables");
            DropTable("dbo.locationTables");
            DropTable("dbo.departmentTables");
            DropTable("dbo.routeTables");
            DropTable("dbo.busTables");
            DropTable("dbo.alternateBusTables");
            DropTable("dbo.actualbusTimes");
        }
    }
}
