namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class location : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.locationTables", "busId", "dbo.busTables");
            DropPrimaryKey("dbo.locationTables");
            AddPrimaryKey("dbo.locationTables", "busId");
            AddForeignKey("dbo.locationTables", "busId", "dbo.busTables", "busId");
            DropColumn("dbo.locationTables", "locationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.locationTables", "locationId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.locationTables", "busId", "dbo.busTables");
            DropPrimaryKey("dbo.locationTables");
            AddPrimaryKey("dbo.locationTables", "locationId");
            AddForeignKey("dbo.locationTables", "busId", "dbo.busTables", "busId", cascadeDelete: true);
        }
    }
}
