namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.userTables", "status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.userTables", "status");
        }
    }
}
