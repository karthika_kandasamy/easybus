﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
   public  class alternateBusDTO
    {
        [Display(Name ="Bus-Number")]
        public string busId { get; set; }
        public string alternateBusId { get; set; }
    }
}
