﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class routeDTO
    {
        [Display(Name = "Route ID")]
        public int routeId { get; set; }
        [Display(Name = "Source")]
        public string source { get; set; }
        [Display(Name = "Destination")]
        public string destination { get; set; }
        [Display(Name = "Via")]
        public string via { get; set; }
        [Display(Name = "Duration")]
        public string plannedTime { get; set; }
    }
}
