﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class departmentDTO
    {
        [Display(Name = "Department ID")]
        public int deptno { get; set; }
        [Display(Name = "Department Name")]
        public string deptname { get; set; }
    }
}
