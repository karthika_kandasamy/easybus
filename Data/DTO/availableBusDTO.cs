﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class availableBusDTO
    {
        [Display(Name ="Bus-Number")]
        public string  busId { get; set; }
        [Display(Name = "Bus Registration Number")]
        public string busNo { get; set; }
        [Display(Name = "Route")]
        public int routeId { get; set; }
        [Display(Name = "Bus Status")]
        public int status { get; set; }
    }
}
