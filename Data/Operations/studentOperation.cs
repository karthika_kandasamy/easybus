﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class studentOperation
    {
       
            dataContext db = new dataContext();

            public bool addstudentTable(studentDTO model)
            {
            try
            {
                studentTable record = new studentTable { userId = model.userId, busId = model.busId, departmentId = model.departmentId, parentName = model.parentName, parentPhonenumber = model.parentPhonenumber, studentName = model.studentName };
                db.studentTable.Add(record);
                db.SaveChanges();
                return true;
            }
            catch(Exception err)
            {
                return false;
            }
     }
        public bool edit(studentDTO model)
        {
        try
        {
            studentTable record = new studentTable { userId = model.userId, busId = model.busId, departmentId = model.departmentId, parentName = model.parentName, parentPhonenumber = model.parentPhonenumber, studentName = model.studentName };
            db.studentTable.Attach(record);
            db.Entry(record).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }
        catch(Exception err)
        {
            return false;
        }
        }

        public IList<StudentView> viewstudentDetails()
         {
            List<StudentView> studentViews = new List<StudentView>();
            IList<studentDTO> students = db.studentTable.Select(s => new studentDTO()
            {

                    userId = s.userId,
                    busId = s.busId,
                    departmentId = s.departmentId,
                    parentName = s.parentName,
                    studentName = s.studentName,
                    parentPhonenumber = s.parentPhonenumber
            }).ToList();

            IList<departmentDTO> dept = db.departmentTable.Select(s => new departmentDTO()
            {
                deptname = s.deptname,
                deptno = s.deptno
            }).ToList();

            foreach(var i in students)
            {
                foreach(var j in dept)
                {
                    if(i.departmentId==j.deptno)
                        studentViews.Add(new StudentView { userId = i.userId, busId = i.busId, departmentName = j.deptname , parentName = i.parentName, studentName = i.studentName, parentPhoneNumber = i.parentPhonenumber });
                }
            }                 

            return studentViews;
            }
    public bool deletestudentDetails(string userId)
    {
        try
        {
            var delete = db.studentTable.FirstOrDefault(x => x.userId == userId);
            db.studentTable.Remove(delete);
            db.SaveChanges();

            var deleteuser = db.userTable.FirstOrDefault(x=> x.userId == userId);
                db.userTable.Remove(deleteuser);
                db.SaveChanges();
                return true;
        }
        catch(Exception err)
        {
            return false;
        }
    }

        public studentDTO editstudentdetails(string userId)
        {
            var edit = db.studentTable.FirstOrDefault(x => x.userId == userId);
            var editdetails = new studentDTO()
            {
                userId = edit.userId,
                studentName = edit.studentName,
                parentName = edit.parentName,
                parentPhonenumber = edit.parentPhonenumber,
                busId = edit.busId,
                departmentId = edit.departmentId
            };
            return editdetails;

        }
        public bool checkUserExistance(string userId)
        {
            try
            {
                IEnumerable<userDTO> user = db.userTable.Select(s => new userDTO()
                {
                    userId = s.userId

                }).ToList();

                foreach (var id in user)
                {
                    if (id.userId == userId)
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}

public class StudentView
{
    [Display(Name = "User ID")]
    public string userId { get; set; }
    [Display(Name = "Student Name")]
    public string studentName { get; set; }
    [Display(Name = "Bus ID")]
    public string busId { get; set; }
    [Display(Name = "Department Name")]
    public string departmentName { get; set; }
    [Display(Name = "Parent Name")]
    public string parentName { get; set; }
    [Display(Name = "Parent Phone Number")]
    public long parentPhoneNumber { get; set; }
}