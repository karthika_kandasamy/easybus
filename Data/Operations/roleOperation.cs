﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class roleOperation
    {
        dataContext dc = new dataContext();
        public IEnumerable<roleDTO> GetAllrole()
        {
            IEnumerable<roleDTO> result = dc.roleTable.Select(s => new roleDTO()
            {
                roleno = s.roleno,
                rolename = s.rolename
            }).ToList();

            return result;
        }
        public bool saveroleTable(roleDTO model)
        {
            try {
                roleTable role = new roleTable();
                role = dc.roleTable.FirstOrDefault(x => x.rolename == model.rolename);
                if (role == null)
                {
                    roleTable result = new roleTable() { roleno = model.roleno, rolename = model.rolename };
                    dc.roleTable.Add(result);
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool deleteroleTable(int roleno)
        {
            try
            {
                var count = dc.userTable.Count(u => u.roleId == roleno);
                if (count == 0)
                {
                    var rt = dc.roleTable.FirstOrDefault(x => x.roleno == roleno);
                    dc.roleTable.Remove(rt);
                    dc.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool editroleTable(int roleno)
        {
            return true;
        }

        public roleDTO Editrole(int roleno)
        {
            var s = dc.roleTable.FirstOrDefault(x => x.roleno == roleno);
            roleDTO dto = new roleDTO();
            dto.roleno = s.roleno;
            dto.rolename = s.rolename;
            return dto;

        }
        public bool updaterole(roleDTO dto)
        {
            roleTable r = new roleTable() { roleno = dto.roleno, rolename = dto.rolename };
            dc.roleTable.Attach(r);
            dc.Entry(r).State = EntityState.Modified;
            dc.SaveChanges();
            return true;


        }
    }

}

