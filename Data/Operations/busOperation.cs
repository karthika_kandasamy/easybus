﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class busOperation
    {
        dataContext bc = new dataContext();

        public bool addBusDetails(busDTO savemodel)
        {
            try
            {
                busTable b = new busTable();
                b = bc.busTable.FirstOrDefault(x => x.busNo == savemodel.busNo);
                if (b == null)
                {
                    busTable record = new busTable() { busId = savemodel.busId, busNo = savemodel.busNo, routeId = savemodel.routeId, status = 0 };
                    bc.busTable.Add(record);
                    bc.SaveChanges();
                    alternateBusTable alterbus = new alternateBusTable() { busId = savemodel.busId, alternateBusId = savemodel.busId };
                    bc.alternateBusTable.Add(alterbus);
                    bc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                return false;
            }
        }

        public bool updateBusDetails(busDTO savemodel)
        {
            try
            {
                busTable record = new busTable() { busId = savemodel.busId, busNo = savemodel.busNo, routeId = savemodel.routeId };
            bc.busTable.Attach(record);
            bc.Entry(record).State = EntityState.Modified;
            bc.SaveChanges();
            return true;
            }
            catch (Exception err)
            {
                return false;
            }
        }

        public IEnumerable<busDTO> GetBusDetails()
        {
            IEnumerable<busDTO> buses = bc.busTable.Select(s => new busDTO()
            {
                busId = s.busId,
                busNo = s.busNo,
                routeId = s.routeId
            }).ToList();

            return buses;
        }

        public IEnumerable<busViewRoute> GetBusDetailsWithRoute()
        {
            IList<busViewRoute> busroute = new List<busViewRoute>();
            IEnumerable<busDTO> buses = bc.busTable.Select(s => new busDTO()
            {
                busId = s.busId,
                busNo = s.busNo,
                routeId = s.routeId
            }).ToList();

            IList<routeDTO> routes = bc.routeTable.Select(s => new routeDTO()
            {
                routeId = s.routeId,
                source = s.source,
                destination = s.destination,
                via = s.via,
                plannedTime = s.plannedTime
            }).ToList();

            foreach (var bus1 in buses)
            {
                foreach (var route1 in routes)
                {
                    if (bus1.routeId == route1.routeId)
                    {
                        busroute.Add(new busViewRoute { busId = bus1.busId, busNo = bus1.busNo, Route = route1.source + " - " + route1.via + " - " + route1.destination,Destination = route1.destination,via = route1.via });
                    }
                }
            }

            return busroute;
        }


        public bool deleteBusDetails(String busId)
        {
            try
            {
                var count = bc.studentTable.Count(u => u.busId == busId);
                if (count == 0)
                {
                    var deleterecord = bc.busTable.FirstOrDefault(x => x.busId.Equals(busId));
                    bc.busTable.Remove(deleterecord);
                    bc.SaveChanges();
                    var deleterecord2 = bc.alternateBusTable.FirstOrDefault(x => x.busId.Equals(busId));
                    bc.alternateBusTable.Remove(deleterecord2);
                    bc.SaveChanges();
                    return true;
                }
                else
                    return false;

            }
            catch (Exception ee)
            {
                return false;
            }
        }

        public busDTO EditUser(String busId)
        {
            try
            {
                var bus = bc.busTable.FirstOrDefault(m => m.busId.Equals(busId));
                busDTO editBus = new busDTO()
                {
                    busId = bus.busId,
                    busNo = bus.busNo,
                    routeId = bus.routeId
                };
           
                return editBus;
            }
            catch(Exception err)
            {
                return null;
            }

        }

        public string statusupdate(string busId)
        {
            try
            {
                //busTable ut = new busTable();
                //ut = bc.busTable.FirstOrDefault(x => x.busId.Equals(busId));
                //ut.status = 1;
                //bc.busTable.Attach(ut);
                //bc.Entry(ut).State = EntityState.Modified;
                //bc.SaveChanges();
                var s = bc.actualbusTimes.FirstOrDefault(x => x.busId == busId);
                if(s==null)
                {
                    actualbusTime abt = new actualbusTime()
                    {
                        busId = busId,
                        starttime = DateTime.Now.ToShortTimeString(),
                        endtime = DateTime.Now.ToShortTimeString()
                    };
                    bc.actualbusTimes.Add(abt);
                    bc.SaveChanges();
                }
                else
                {
                    s.starttime = DateTime.Now.ToShortTimeString();
                    s.endtime = DateTime.Now.ToShortTimeString();
                    bc.actualbusTimes.Attach(s);
                    bc.Entry(s).State = EntityState.Modified;
                    bc.SaveChanges();
                }
                return "true";
            }
            catch(Exception e)
            {
                return e.ToString();
            }
        }
        public bool statusreverse(string busId)
        {
            try { 
            busTable ut = bc.busTable.FirstOrDefault(x => x.busId.Equals(busId));
            ut.status = 0;
            bc.busTable.Attach(ut);
            bc.Entry(ut).State = EntityState.Modified;
            bc.SaveChanges();

            var s = bc.actualbusTimes.FirstOrDefault(x => x.busId == busId);
                
                bc.actualbusTimes.Attach(s);
                bc.Entry(s).State = EntityState.Modified;
                bc.SaveChanges();
                return true;
        }
            catch(Exception e)
            {
                return false;
            }
}
        public IEnumerable<availableBusDTO> availBusDetails()
        {
            IEnumerable<availableBusDTO> buses = bc.busTable.Select(s => new availableBusDTO()
            {
                busId = s.busId,
                busNo = s.busNo,
                routeId = s.routeId,
                status = s.status
            }).ToList();

            return buses;
        }
        public IEnumerable<alternateBusDTO> alterReady()
        {

            IEnumerable<alternateBusDTO> buses = bc.alternateBusTable.Select(s => new alternateBusDTO()
            {
                busId = s.busId,
                alternateBusId = s.alternateBusId
            }).ToList();
            return buses;
        }







        public bool updatealterbus(String busId)
        {
            try
            {
                var br = bc.busTable.FirstOrDefault(x => x.busId.Equals(busId));
                br.status = 0;
                bc.busTable.Attach(br);
                bc.Entry(br).State = EntityState.Modified;
                bc.SaveChanges();

                var bus = bc.alternateBusTable.FirstOrDefault(m => m.busId.Equals(busId));
                bus.alternateBusId = bus.busId;

                bc.alternateBusTable.Attach(bus);
                bc.Entry(bus).State = EntityState.Modified;
                bc.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}

public class busViewRoute
{
    [Display(Name = "Bus ID")]
    public string busId { get; set; }
    [Display(Name = "Bus Number")]
    public string busNo { get; set; }
    [Display(Name = "Route")]
    public string Route { get; set; }
    public string Destination { get; set; }
    public string via { get; set; }
    //public int status { get; set; }
}
