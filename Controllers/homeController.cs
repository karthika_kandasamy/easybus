﻿using Data;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace easyBus2.Controllers
{
    public class homeController : Controller
    {
        // GET: home
        dataContext dc = new dataContext();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult adminvalidate(string username,string password)
        {
            var result = dc.userTable.FirstOrDefault(x => x.userName == username && x.password == password);
            if (result != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
           
        }
    }
}